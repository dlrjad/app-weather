import Home from './App.vue';
import Municipio from './components/Municipio.vue';
import Temperatura from './components/Temperatura.vue';

export const routes = [
  {path: '/', component: Home},
  /*{path: '/municipio/:id', component: Municipio, name: 'municipio', children: [
    {path: 'temperatura/:id/:id_municipio', component: Temperatura, name: 'temperatura'},
  ]},*/
  {path: '/municipio/:id', component: Municipio, name: 'municipio'},
  {path: 'temperatura/:id/:id_municipio', component: Temperatura, name: 'temperatura'},
  
];