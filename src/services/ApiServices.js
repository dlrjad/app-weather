import axios from 'axios'
export default class RestResource {

  getProvinces() {
    return new Promise((resolve, reject) => {
      axios.get('https://www.el-tiempo.net/api/json/v1/provincias').then(
        response => {
          //console.log(respuesta.data
          let myData = response.data.map(e => {
            return {
              CODPROV: e.CODPROV,
              NOMBRE_PROVINCIA: e.NOMBRE_PROVINCIA
            };
          });

          resolve(myData);
          //console.info('Provincias obtenidas')
        },
        error => {
          reject('No se pudieron obtener las provincias');
          //console.error("hubo un fallo en la peticion", error);
        },

        () => {
          console.info("Peticion completa");
        })
    })
  }

  getmunicipalities(idProvince) {
    return new Promise((resolve, reject) => {
      axios.get('https://www.el-tiempo.net/api/json/v1/provincias/'+idProvince+'/municipios').then((respuesta) => {
        resolve(respuesta.data)
      });
    }) 
  }

  getTemperature(idProvince, idMunicipality) {
    return new Promise((resolve, reject) => {
      axios.get('https://www.el-tiempo.net/api/json/v1/provincias/'+idProvince+'/municipios/'+idMunicipality+'/weather').then((respuesta) => {
        resolve(respuesta.data.prediccion.dia.splice(0, 3))
        /*Object.values(respuesta).map((e) => {
          console.log(e.response)
        });*/
      })
    })
  }

}