export default { 
  es: {
    message: {
      Title: 'Aplicación del tiempo',
      Language: 'Idioma',
      Provinces: 'Provincias',
      Municipalities: 'Municipios',
      Date: 'Fecha',
      Temperature: 'Temperatura',
      ThermalSensation: 'Sesación térmica',
      RelativeHumidity: 'Humedad relativa',
      Maximun: 'Máximo',
      Minimun: 'Mínimo',
    }   
  } 
}
