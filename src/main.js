import Vue from 'vue'
import App from './App.vue'
import {routes} from './routes'
import VueRouter from 'vue-router'

import {i18n} from './constant/vue-i18n';

Vue.use(VueRouter);

const router = new VueRouter({
  routes: routes
});

export const eventBus = new Vue();

new Vue({
  el: '#app',
  i18n,
  render: h => h(App)
})
