export default { 
  en: {
    message: {
      Title: 'Weather app',
      Language: 'Language',
      Provinces: 'Provinces',
      Municipalities: 'Municipalities',
      Date: 'Date',
      Temperature: 'Temperature',
      ThermalSensation: 'Thermal sensation',
      RelativeHumidity: 'Relative humidity',
      Maximun: 'Maximun',
      Minimun: 'Minimun',
    }   
  } 
}